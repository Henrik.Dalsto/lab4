package no.uib.inf101.gridview;

import java.awt.Color;
import javax.swing.JFrame;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    ColorGrid grid = new ColorGrid(3, 4);
    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    GridView g = new GridView(grid);
    JFrame f = new JFrame();
    f.setContentPane(g);
    f.setTitle("My Awesome Graphix");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.pack();
    f.setVisible(true);



  }




}
