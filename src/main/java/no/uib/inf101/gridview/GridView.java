package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import javax.swing.JPanel;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {

    IColorGrid grid;
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    public GridView(IColorGrid grid) {
        this.grid = grid;
        this.setPreferredSize(new Dimension(400, 300));
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGrid(g2);

    }

    private void drawGrid(Graphics2D g2D){
        double width = this.getWidth() - 2 * OUTERMARGIN;
        double height = this.getHeight() - 2 * OUTERMARGIN;
        Rectangle2D r2d2 = new Double(OUTERMARGIN, OUTERMARGIN,width,height);
        g2D.setColor(MARGINCOLOR);
        g2D.fill(r2d2);
        CellPositionToPixelConverter pixler = new CellPositionToPixelConverter(r2d2,grid,OUTERMARGIN);
        drawCells(g2D, grid, pixler);
    }


    private static void drawCells(Graphics2D c, CellColorCollection colors, CellPositionToPixelConverter pixler){
        for(CellColor cell: colors.getCells()){
            if(cell.color()==null){
                c.setColor(Color.DARK_GRAY);
            } else{
                c.setColor(cell.color());
            }
            c.fill(pixler.getBoundsForCell(cell.cellPosition()));
            c.draw(pixler.getBoundsForCell(cell.cellPosition()));
        }
    }


}
