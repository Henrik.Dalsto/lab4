package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

    private final int rows;
    private final int cols;

    private final List<List<CellColor>> grid;

    public ColorGrid(int rows, int cols) throws IllegalArgumentException {
        if ((rows <= 0) || (cols <= 0)) {
            throw new IllegalArgumentException(
                    "Grid needs at least 1 row and 1 column, you supplied " + rows + " rows and " +
                            cols + " columns");
        }
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>(rows);
        for (int i = 0; i < rows; i++) {
            List<CellColor> row = new ArrayList<>(cols);
            for (int j = 0; j < cols; j++) {
                CellPosition pos = new CellPosition(i, j);
                row.add(new CellColor(pos, null));
            }
            grid.add(row);
        }
    }


    @Override
    public List<CellColor> getCells() {
        List<CellColor> cells = new ArrayList<>();
        for (List<CellColor> cellColors : grid) {
            cells.addAll(cellColors);
        }
        return cells;
    }


    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }

    @Override
    public Color get(CellPosition pos) throws IndexOutOfBoundsException {
        if (isNotOnGrid(pos)) {
            throw new IndexOutOfBoundsException("Out of bounds");
        }
        return grid.get(pos.row()).get(pos.col()).color();
    }

    @Override
    public void set(CellPosition pos, Color color) throws IndexOutOfBoundsException {
        checkPosition(pos);
        grid.get(pos.row()).set(pos.col(), new CellColor(pos, color));

    }

    public void checkPosition(CellPosition pos) throws IndexOutOfBoundsException {
        if (isNotOnGrid(pos)) {
            throw new IndexOutOfBoundsException("Row and column indices must be within bounds");
        }
    }

    public boolean isNotOnGrid(CellPosition pos) {
        return pos.row() < 0 || pos.row() >= rows() || pos.col() < 0 || pos.col() >= cols();
    }

}
